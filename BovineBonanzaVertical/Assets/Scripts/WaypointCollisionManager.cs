﻿using UnityEngine;
using HoloToolkit.Unity;

namespace AggieChallenge.AR
{ 
    public class WaypointCollisionManager : MonoBehaviour {

        private void OnTriggerEnter(Collider other)
        {
            AudioClip clip = AssetChooser.Instance.findClip(this.gameObject);
            AudioSource.PlayClipAtPoint(clip, this.gameObject.transform.position);

            if (other.gameObject.tag == "Enemy") // enemy and waypoint/health
            {
                LivesManager.Instance.LoseLife();

                EnemySpawner.Instance.DespawnEnemy();
                EnemySpawner.Instance.SpawnEnemy();
            }
            else if (this.gameObject.tag == "Health") // hand and health
            {
                LivesManager.Instance.AddLife();

                EnemySpawner.Instance.DespawnEnemy();
                Destroy(this.gameObject);
                WaypointSpawner.Instance.SpawnWaypoint();
                EnemySpawner.Instance.SpawnEnemy();
            }
            else if (this.gameObject.tag == "Bonus") // hand and bonus
            {
                if(other.gameObject.tag == "Hand") ScoreManager.Instance.AddScore(1);

                //EnemySpawner.Instance.DespawnEnemy();
                Destroy(this.gameObject);
                //WaypointSpawner.Instance.SpawnWaypoint();
            }    
            else // hand and regular waypoint
            {
                if (other.gameObject.tag == "Bonus") return;
                ScoreManager.Instance.AddScore(5);

                EnemySpawner.Instance.DespawnEnemy();
                Destroy(this.gameObject);
                WaypointSpawner.Instance.SpawnWaypoint();
                EnemySpawner.Instance.SpawnEnemy();
            }

                   
        }
    }   
}
