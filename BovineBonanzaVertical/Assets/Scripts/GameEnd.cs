﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEnd : MonoBehaviour
{
    private Text gameOverText;

    // Use this for initialization
    void Start()
    {
        gameOverText = (Text)GetComponentInParent(typeof(Text));
        gameOverText.text = "Game Over" +
                            "\nYour Score: " + PlayerPrefs.GetInt("Score").ToString() +
                            "\nHigh Score: " + PlayerPrefs.GetInt("High Score").ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
