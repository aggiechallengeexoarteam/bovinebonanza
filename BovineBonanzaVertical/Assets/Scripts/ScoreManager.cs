﻿using UnityEngine;
using UnityEngine.UI;
using HoloToolkit.Unity;
using UnityEngine.SceneManagement;

namespace AggieChallenge.AR
{
    public class ScoreManager : Singleton<ScoreManager>
    {
        public GameObject Score;
        public GameObject HighScore;
        public int score { get; private set; }
        public int highScore { get; private set; }

        private Text scoreText;
        private Text highScoreText;

        void Start()
        {
            SceneManager.sceneLoaded += onSceneLoaded;
            score = 0;
            highScore = PlayerPrefs.GetInt("High Score");
            scoreText = Score.GetComponent<Text>();
            highScoreText = HighScore.GetComponent<Text>();

            scoreText.text = "SCORE" + "  " + score;
        }

        void Update()
        {
            scoreText.text = "SCORE" + "  " + score;
            highScoreText.text = "HIGH SCORE" + "    " + highScore;
        }

        void onSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (scene.name == "Report") {
                PlayerPrefs.SetInt("Score", (int)score);
                if (score > PlayerPrefs.GetInt("High Score"))
                {
                    PlayerPrefs.SetInt("High Score", (int)score);
                }

                PlayerPrefs.Save(); //invisible
            }
        }

        public void AddScore(int newScoreValue)
        {
            score += newScoreValue;

            if (score > highScore)
            {
                highScore = score;
            }
        }

        protected override void OnDestroy()
        {
            //if (score == highScore)
            //{
            //    Data.Save(score);
            //    Data.SaveTime((int)Timer.Instance.getTime());
            //}

            //Data.SavePreviousScore(score, (int)Timer.Instance.getTime());
            //Data.SavePreviousDifficulty();
        }
    }
}

