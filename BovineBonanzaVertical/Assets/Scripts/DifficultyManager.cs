﻿using System.Collections;
using System.Collections.Generic;
using HoloToolkit.Unity;
using AggieChallenge.AR;
using UnityEngine;

namespace AggieChallenge.AR
{
    public class DifficultyManager : Singleton<DifficultyManager>
    {
        private string difficulty; //uninitialized until game can receive difficulty instructions from portal

        public Dictionary<string, float> speed = new Dictionary<string, float>()
        {
            {"easy", .005f},
            {"medium", .01f },
            {"hard", .03f }
        };

        public Dictionary<string, float> healthChance = new Dictionary<string, float>()
        {
            {"easy", .5f},
            {"medium", .33f },
            {"hard", .25f }
        };

        public Dictionary<string, float> bonusChance = new Dictionary<string, float>()
        {
            {"easy", .3f},
            {"medium", .2f },
            {"hard", .1f }
        };

        public Dictionary<string, int> maxExercises = new Dictionary<string, int>()
        {
            {"easy", 3 },
            {"medium", 5 },
            {"hard", 7 }
        };

        void Start()
        {
            if (difficulty == "")
                difficulty = "medium";
        }

        public float getSpeed()
        {
            return speed[difficulty];
        }

        public float getHealthChance()
        {
            return healthChance[difficulty];
        }

        public float getBonusChance()
        {
            return bonusChance[difficulty];
        }

        public int getMaxExercises()
        {
            return maxExercises[difficulty];
        }
    } 
}
