﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

public class HandObjectManager : MonoBehaviour
{
    public float speed = 0.01f;

    private void Start()
    {
        this.transform.position = new Vector3(0, 0, 0.2f);
    }

    void Update()
    {
        Vector3 translation = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), Input.GetAxis("Depth"));
        translation *= speed;
        this.transform.Translate(translation);
    }

    //public string cleverarmAddr = "10.230.155.177";
    //public float offset = 15;

    //private TcpClient client;
    //private Vector3 handOffset = Vector3.zero;
    //private bool synced = false;

    //// Update is called once per frame
    //void Update()
    //{
    //    client = new TcpClient();
    //    client.Connect(cleverarmAddr, 10000);
    //    Byte[] bytes = new Byte[256];
    //    if (!client.Connected)
    //    {
    //        Debug.Log("cannot connect to CLEVERarm");
    //        return;
    //    }
    //    NetworkStream stream = client.GetStream();
    //    int i;
    //    // Loop to receive all the data sent by the client.
    //    string rawData = "";
    //    List<Double> data = new List<double>();
    //    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
    //    {
    //        rawData = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
    //        //Debug.Log("Received: " + rawData);
    //    }
    //    string[] parsedData = rawData.Split(' ');
    //    for (int x = 0; x < 4; x++)
    //    {
    //        //Debug.Log(parsedData[x] + " : " + parsedData[x].Length);
    //        try
    //        {
    //            double temp = (Convert.ToDouble(parsedData[x]));
    //            data.Add(temp);
    //        }
    //        catch (Exception e) { }
    //    }

    //    // might need negation
    //    Vector3 handPos = new Vector3();
    //    handPos.x = -1 * (float)data[1];
    //    handPos.y = (float)data[0];
    //    handPos.z = -1 * (float)data[2];
    //    handPos *= offset;
    //    if (!synced)
    //    {
    //        handOffset = handPos - this.transform.position;
    //        synced = true;
    //    }
    //    else
    //    {
    //        this.transform.position = handPos - handOffset;
    //    }
    //}
}
