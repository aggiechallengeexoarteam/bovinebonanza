﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HoloToolkit.Unity;
using System;

namespace AggieChallenge.AR
{
    public class EnemySpawner : Singleton<EnemySpawner>
    {
        public float enemySpawnDistance;
        public GameObject currentEnemy { get; private set; }

        private GameObject enemyPrefab;

        void Start()
        {
            enemyPrefab = AssetChooser.Instance.enemyAsset.prefab;
            SpawnEnemy();
        }

        public void SpawnEnemy() {
            if (WaypointSpawner.Instance.currentWaypoint.tag == "Bonus")
                return;

            Vector3 waypointPosition = WaypointSpawner.Instance.currentWaypoint.transform.position;
            currentEnemy = Instantiate (enemyPrefab, new Vector3(0, -1, enemySpawnDistance), Quaternion.LookRotation(waypointPosition, Vector3.up));
            currentEnemy.transform.rotation = Quaternion.AngleAxis(90, Vector3.up);
        }

        public void DespawnEnemy()
        {
            if (currentEnemy)
            {
                Timer.Instance.stopTimer();
                Destroy(currentEnemy); 
            }
        }
    } 
}