﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace AggieChallenge.AR
{
    public class ReportManager : Singleton<ReportManager>
    {
        public Text easyReport;
        public Text mediumReport;
        public Text hardReport;

        void Start()
        {
            LoadProgressReport();
        }

        private void LoadProgressReport()
        {
            //easyReport.text = "Easy: " + PlayerPrefs.GetInt(Data.patient + "_" + Data.level + "_easy", 0) + " / " +
            //                                PlayerPrefs.GetInt(Data.patient + "_" + Data.level + "_easy_time", 0);
            //mediumReport.text = "Medium: " + PlayerPrefs.GetInt(Data.patient + "_" + Data.level + "_medium", 0) + " / " +
            //                                  PlayerPrefs.GetInt(Data.patient + "_" + Data.level + "_medium_time", 0);
            //hardReport.text = "Hard: " + PlayerPrefs.GetInt(Data.patient + "_" + Data.level + "_hard", 0) + " / " +
            //                                PlayerPrefs.GetInt(Data.patient + "_" + Data.level + "_hard_time", 0);
        }
    } 
}
