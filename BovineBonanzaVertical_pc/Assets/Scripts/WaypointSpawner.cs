﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using UnityEngine.SceneManagement;

namespace AggieChallenge.AR
{
    public class WaypointSpawner : Singleton<WaypointSpawner>
    {
        public GameObject[] waypointLocationsObjects;
        public GameObject currentWaypoint { get; private set; }

        public GameObject[] bonusPoints;
        public Queue<GameObject> spawnQueue { get; private set; }

        public int numExercises;

        private Vector3[] waypointLocations;
        private int exercisesSpawned;
        private int currentLocationIndex;
        
        void Start()
        {
            spawnQueue = new Queue<GameObject>();
            exercisesSpawned = 0;
            currentLocationIndex = 0;
            readWaypointLocations();
            SpawnWaypoint(); 
        }

        public void queueExercise(string waypointType)
        {
            exercisesSpawned++;
            destroyBonusPoints();
            if (exercisesSpawned == numExercises+1)
            {
                Timer.Instance.stopTimer();
                SceneManager.LoadScene("Scenes/Report");              
            }

            foreach(Vector3 location in waypointLocations)
            {
                queueWaypoint(waypointType);
            }
        }

        public void queueWaypoint(string waypointType)
        {
            spawnQueue.Enqueue(findAsset(waypointType));
        }

        public void SpawnWaypoint()
        {
            if (spawnQueue.Count == 0)
            {
                string nextWaypoint = getNextWaypointType();
                if (nextWaypoint == "Health")
                    queueWaypoint(nextWaypoint);
                else
                    queueExercise(nextWaypoint);
            }

            Vector3 prevPos = Vector3.zero;
            if (currentWaypoint != null)
                prevPos = currentWaypoint.transform.position;

            if (prevPos != Vector3.zero)
                spawnBonusPointsLine(prevPos, waypointLocations[currentLocationIndex]);

            currentWaypoint = Instantiate(spawnQueue.Dequeue(), 
                                          waypointLocations[currentLocationIndex], 
                                          Quaternion.LookRotation(Camera.main.transform.position-waypointLocations[currentLocationIndex],Vector3.up));

            currentLocationIndex++;
            if (currentLocationIndex == waypointLocations.Length)
                currentLocationIndex = 0;
        }

        private GameObject findAsset(string name)
        {
            for (int i = 0; i < AssetChooser.Instance.waypointAssets.Length; ++i)
            {

                GameObject asset = AssetChooser.Instance.waypointAssets[i].prefab;
                if (asset.tag == name)
                {
                    return asset;
                }
            }

            return null;
        }

        private string getNextWaypointType()
        {
            float rand = Random.Range(0f, 1f);
            if (LivesManager.Instance.lives == 1 && rand <= 0.1)
            {
                return "Health";
            }
            /*else if (exercisesSpawned>0 && rand <= 0.1)
            {
                return "Bonus";
            }*/

            return "Waypoint";
        }

        private void readWaypointLocations()
        {
            waypointLocations = new Vector3[waypointLocationsObjects.Length];
            for(int i = 0; i < waypointLocations.Length; i++)
            {
                waypointLocations[i] = waypointLocationsObjects[i].transform.position;
            }
        }

        private void spawnBonusPointsLine(Vector3 oldPos, Vector3 newPos)
        {
            Vector3 oldToNew = newPos - oldPos;
            oldToNew /= bonusPoints.Length;
            Vector3 bPos;
            for(int i = 0; i < bonusPoints.Length; i++)
            {
                bPos = oldPos + oldToNew * i;
                if (bonusPoints[i] != null) Destroy(bonusPoints[i]);
                bonusPoints[i] = Instantiate(findAsset("Bonus"),
                                            bPos,
                                            Quaternion.LookRotation(Camera.main.transform.position - bPos, Vector3.up));

            }
        }

        private void destroyBonusPoints()
        {
            for (int i = 0; i < bonusPoints.Length; i++)
            {
                if (bonusPoints[i] != null) Destroy(bonusPoints[i]);
            }
        }

    }
}
