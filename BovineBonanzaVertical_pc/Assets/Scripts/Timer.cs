﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HoloToolkit.Unity;

namespace AggieChallenge.AR
{
    public class Timer : Singleton<Timer>
    {
        public Slider timeSlider;
        public bool on { get; set; }

        private float greenValue = 255f;
        private float totalTime;

        void Start()
        {
            totalTime = 0;
            on = false;
        }

        void FixedUpdate()
        {
            totalTime += Time.deltaTime;
            if (on)
            {               
                timeSlider.value -= Time.deltaTime;
                greenValue = clamp(timeSlider.value * (255f / timeSlider.maxValue), 255f, 0);
                greenValue = ((greenValue * 100) / 255f) / 100;
                if(timeSlider)
                    timeSlider.fillRect.GetComponent<Image>().color = new Color(1 - greenValue + 0.2f, greenValue + 0.2f, 0, 0.9f);
            }
        }

        public void startTimer(float time)
        {
            timeSlider.maxValue = time;
            timeSlider.value = time;
            on = true;
        }

        public void stopTimer()
        {
            timeSlider.value = timeSlider.maxValue;
            on = false;
        }

        public float getTime()
        {
            return totalTime;
        }

        private float clamp(float value, float max, float min)
        {
            if (value > max)
            {
                return max;
            }
            else if (value < min)
            {
                return min;
            }
            else
            {
                return value;
            }
        }
    }

}