﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameStart : MonoBehaviour {

    public float countdownTime;
    private float timer;
    private Text countdownText;

	// Use this for initialization
	void Start () {
        timer = countdownTime;
        countdownText = GetComponentInParent<Text>();
        countdownText.text = "";
	}
	
	// Update is called once per frame
	void Update () {
        if(timer<=0)
        {
            SceneManager.LoadScene("paths1");
        }

		if(HandsTrackingController.Instance.trackedHand)
        {
            timer -= Time.deltaTime;
            countdownText.text = "Game begins in: " + ((int)(timer+1)).ToString();
        }
        else
        {
            timer = countdownTime;
            countdownText.text = "";
        }
	}
}
