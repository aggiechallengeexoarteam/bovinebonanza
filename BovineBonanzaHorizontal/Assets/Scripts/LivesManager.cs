﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using HoloToolkit.Unity;
using AggieChallenge.AR;

public class LivesManager : Singleton<LivesManager>
{

    public int maxLives;
    public GameObject Hearts;
    public int lives { get; private set; }
 	
    void Start()
    {
        lives = maxLives;
    }

    public void LoseLife()
    {
        Hearts.transform.GetChild(lives - 1).gameObject.SetActive(false);
        lives--;
    }

    public void AddLife()
    {
        if (lives == maxLives)
        {
            ScoreManager.Instance.AddScore(2);
        }
        else
        {
            Hearts.transform.GetChild(lives).gameObject.SetActive(true);
            lives++; 
        }   
    }

    void Update()
    {
        if (lives == 0 && SceneManager.GetActiveScene().name == "paths1")
        {
            SceneManager.LoadScene("Scenes/Report");
        }
    }
}
