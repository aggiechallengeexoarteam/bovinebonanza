﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.

using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.WSA.Input;

/// <summary>
/// HandsManager determines if the hand is currently detected or not.
/// </summary>
public class HandsTrackingController : Singleton<HandsTrackingController>
{
    /// <summary>
    /// HandDetected tracks the hand detected state.
    /// Returns true if the list of tracked hands is not empty.
    /// </summary>
    public bool HandDetected
    {
        get { return trackedHand != null; }
    }

    private GameObject TrackingPrefab;
    public Color DefaultColor = Color.green;
    public Color TapColor = Color.blue;
    public Color HoldColor = Color.red;

    public GameObject trackedHand { get; private set; }
    private GestureRecognizer gestureRecognizer;
    private uint activeId;

    void Awake()
    {
        SceneManager.sceneLoaded += onSceneLoaded;

        InteractionManager.InteractionSourceDetected += InteractionManager_InteractionSourceDetected;
        InteractionManager.InteractionSourceUpdated += InteractionManager_InteractionSourceUpdated;
        InteractionManager.InteractionSourceLost += InteractionManager_InteractionSourceLost;

        gestureRecognizer = new GestureRecognizer();
        gestureRecognizer.SetRecognizableGestures(GestureSettings.Tap | GestureSettings.Hold);
        gestureRecognizer.Tapped += GestureRecognizerTapped;
        gestureRecognizer.HoldStarted += GestureRecognizer_HoldStarted;
        gestureRecognizer.HoldCompleted += GestureRecognizer_HoldCompleted;
        gestureRecognizer.HoldCanceled += GestureRecognizer_HoldCanceled;            
        gestureRecognizer.StartCapturingGestures();
        // StatusText.text = "READY";
    }

    void onSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "tutorial")
            TrackingPrefab = new GameObject(); //invisible
        else
        {
            TrackingPrefab = AssetChooser.Instance.handAsset.prefab;
            if (HandDetected)
            {
                var obj = Instantiate(TrackingPrefab) as GameObject;
                obj.transform.position = trackedHand.transform.position;
                trackedHand = obj;
            }
        }
    }

    //void ChangeObjectColor(GameObject obj, Color color)
    //{            
    //    if(obj != null)
    //    {
    //        var rend = obj.GetComponentInChildren<Renderer>();
    //        if (rend)
    //        {
    //            rend.material.color = color;
    //            Debug.LogFormat("Color Change: {0}", color.ToString());
    //        }
    //    }      
    //}


    private void GestureRecognizer_HoldStarted(HoldStartedEventArgs args)
    {
        uint id = args.source.id;            
        //StatusText.text = $"HoldStarted - Kind:{args.source.kind.ToString()} - Id:{id}";

        //ChangeObjectColor(trackedHand, HoldColor);
        //StatusText.text += "-TRACKED";
    }

    private void GestureRecognizer_HoldCompleted(HoldCompletedEventArgs args)
    {
        uint id = args.source.id;            
        //StatusText.text = $"HoldCompleted - Kind:{args.source.kind.ToString()} - Id:{id}";
        //ChangeObjectColor(trackedHand, DefaultColor);
        //StatusText.text += "-TRACKED";
    }

    private void GestureRecognizer_HoldCanceled(HoldCanceledEventArgs args)
    {
        uint id = args.source.id;            
        //StatusText.text = $"HoldCanceled - Kind:{args.source.kind.ToString()} - Id:{id}";

        //ChangeObjectColor(trackedHand, DefaultColor);
        //StatusText.text += "-TRACKED";
    }

    private void GestureRecognizerTapped(TappedEventArgs args)
    {            
        uint id = args.source.id;
        //StatusText.text = $"Tapped - Kind:{args.source.kind.ToString()} - Id:{id}";

        //ChangeObjectColor(trackedHand, TapColor);
        //StatusText.text += "-TRACKED";           
    }
        

    private void InteractionManager_InteractionSourceDetected(InteractionSourceDetectedEventArgs args)
    {
        uint id = args.state.source.id;
        // Check to see that the source is a hand.
        if (args.state.source.kind != InteractionSourceKind.Hand)
        {
            return;
        }

        if (trackedHand == null)
        {
            var obj = Instantiate(TrackingPrefab) as GameObject;
            obj.DontDestroyOnLoad();
            Vector3 pos;

            if (args.state.sourcePose.TryGetPosition(out pos))
            {
                obj.transform.position = pos;
            }

            trackedHand = obj; 
        }
    }

    private void InteractionManager_InteractionSourceUpdated(InteractionSourceUpdatedEventArgs args)
    {
        uint id = args.state.source.id;
        Vector3 pos;
        Quaternion rot;

        if (args.state.source.kind == InteractionSourceKind.Hand)
        {
            if (args.state.sourcePose.TryGetPosition(out pos) && trackedHand != null)
            {
                trackedHand.transform.position = pos;
            }

            if (args.state.sourcePose.TryGetRotation(out rot) && trackedHand != null)
            {
                trackedHand.transform.rotation = rot;
            }
        }
    }

    private void InteractionManager_InteractionSourceLost(InteractionSourceLostEventArgs args)
    {
        uint id = args.state.source.id;
        // Check to see that the source is a hand.
        if (args.state.source.kind != InteractionSourceKind.Hand)
        {
            return;
        }

        if(trackedHand != null)
            Destroy(trackedHand);
    }

    override protected void OnDestroy()
    {                        
        InteractionManager.InteractionSourceDetected -= InteractionManager_InteractionSourceDetected;
        InteractionManager.InteractionSourceUpdated -= InteractionManager_InteractionSourceUpdated;
        InteractionManager.InteractionSourceLost -= InteractionManager_InteractionSourceLost;

        gestureRecognizer.Tapped -= GestureRecognizerTapped;
        gestureRecognizer.HoldStarted -= GestureRecognizer_HoldStarted;
        gestureRecognizer.HoldCompleted -= GestureRecognizer_HoldCompleted;
        gestureRecognizer.HoldCanceled -= GestureRecognizer_HoldCanceled;
        gestureRecognizer.StopCapturingGestures();
    }
}